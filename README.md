# mean-project

MEAN project

- SPA
- Authentication
- Authorization
- Internationalization (ngx-translate)
- Angular Material (with i18n)
- AWS

## Installation

```javascript
npm install
```

## Usage Client

```javascript
ng serve
```

## Usage Server

```javascript
npm run start:server
```

## user auth

test123@test.com
123456

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
