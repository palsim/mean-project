import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
} from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { PaginatorI18n } from './paginator-i18n';
import { CustomTranslateService } from './custom-translate.service';

@NgModule({
  exports: [
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSelectModule,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      deps: [CustomTranslateService],
      useFactory: (translateService: CustomTranslateService) =>
        new PaginatorI18n(translateService).getPaginatorIntl(),
    },
  ],
})
export class AngularMaterialModule {}
