import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(private authService: AuthService,
    public translate: TranslateService) {
      translate.addLangs(['en', 'pt']);
      translate.setDefaultLang('en');

      const lang = localStorage.getItem('lang');
      if(lang) {
        translate.use(lang);      
      } else {
        const browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|pt/) ? browserLang : 'en');
        localStorage.setItem('lang', translate.currentLang);
      }      
    }
  
  ngOnInit() {
    this.authService.autoAuthUser();
  }
}
