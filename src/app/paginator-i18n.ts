//import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { CustomTranslateService } from './custom-translate.service';

//@Injectable()
export class PaginatorI18n {
  private paginatorIntl: MatPaginatorIntl;
  private switchLangListenerSubs: Subscription;

  constructor(private translateService: CustomTranslateService) {
    this.paginatorIntl = new MatPaginatorIntl();

    this.switchLangListenerSubs = this.translateService
      .getSwitchLangListener()
      .subscribe(() => {
        this.getPaginatorIntl();
        this.paginatorIntl.changes.next();
      });
  }

  getPaginatorIntl(): MatPaginatorIntl {
    this.paginatorIntl.itemsPerPageLabel = this.translateService.instant(
      'PAGINATOR.ITEMS_PER_PAGE_LABEL'
    );
    this.paginatorIntl.nextPageLabel = this.translateService.instant(
      'PAGINATOR.NEXT_PAGE_LABEL'
    );
    this.paginatorIntl.previousPageLabel = this.translateService.instant(
      'PAGINATOR.PREVIOUS_PAGE_LABEL'
    );
    this.paginatorIntl.firstPageLabel = this.translateService.instant(
      'PAGINATOR.FIRST_PAGE_LABEL'
    );
    this.paginatorIntl.lastPageLabel = this.translateService.instant(
      'PAGINATOR.LAST_PAGE_LABEL'
    );
    return this.paginatorIntl;
  }
}
