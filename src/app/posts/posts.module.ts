//import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
//import { TranslateModule } from "@ngx-translate/core";
import { AngularMaterialModule } from "../angular-material.module";
import { SharedModule } from "../shared.module";
import { PostCreateComponent } from "./post-create/post-create.component";
import { PostListComponent } from "./post-list/post-list.component";


@NgModule({
    declarations: [
        PostCreateComponent,
        PostListComponent
    ],
    imports: [
        //CommonModule,
        SharedModule,
        ReactiveFormsModule,
        RouterModule,
        AngularMaterialModule,
        //TranslateModule
    ]
})
export class PostsModule { }