import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CustomTranslateService {
  
  private switchLangListener = new Subject();

    public get currentLang() {
        return this.translateService.currentLang;
    }

    constructor(public translateService: TranslateService) { }

  getSwitchLangListener() {
    return this.switchLangListener.asObservable();
  }

  getLangs() {
    return this.translateService.getLangs();
  }

  switchLang(lang: string) {    
    this.translateService.use(lang).subscribe(() => {
      localStorage.setItem('lang', lang);
      this.switchLangListener.next();    
    });    
  }

  instant(key: string){
    return this.translateService.instant(key);
  }
}
